#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <ios>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <iostream>
#include <cmath>

using namespace std;

typedef long long ll;
#define vecvec(a) vector< vector< a > >

char menorChar = ' ' - 1;

struct euc{
	ll inv;
	ll gcd;
};

euc make_euc(ll inv, ll gcd){
	euc a = {inv, gcd};
	return a;
}

euc euclides_ext(ll a,ll b){
	ll r0 = a, r1 = b, r2;
	ll q;
	ll m0 = 1, m1 = 0, m2;

	while(r0 != 0){
		r2 = r1;
		r1 = r0;
		m2 = m1;
		m1 = m0;
		q  = r2 / r1;
		r0 = r2 % r1;
		m0 = m2 - (m1 * q);
	}

	m1 %= b;
	while(m1 < 0){
		m1 += b;
	}

	return make_euc(m1, r1);
}

struct key{
	ll n;
	ll e;
	ll d;
};

key make_key(ll n, ll e, ll d){
	key a = {n, e, d};
	return a;
}

void printvec(vector<ll> a){
	for(int i = 0; i < a.size(); i++){
		cout<<a[i]<<' ';
	}
	cout<<endl;
}

ll powr(ll a, ll b){
	if(b == 0){
		return 1;
	}else if(b % 2 == 0){
		return powr(a*a, b>>1);
	}else{
		return powr(a*a, b>>1)*a;
	}
}

ll modpowr(ll a, ll b, ll n){
	ll resul = 1;
	while(b--){
		resul *= a;
		resul %= n;
	}
	return resul;
	// if(b == 0){
		// return 1;
	// }else if(b % 2 == 0){
		// return modpowr(a*a % n, b>>1, n) % n;
	// }else{
		// return modpowr(a*a % n, b>>1, n)*a % n;
	// }
}

//! Geracao de Chaves

void entropy(){
	int num;
	cout<<"Numero aleatorio para entorpia?"<<endl;
	cin>>num;
	while(num--){
		rand();
	}

}

ll llrand(ll lim){
	ll resul;
	for(int i = 0; i < 1000; i++){
		resul += (ll) rand();
		resul *= (ll) rand() + 1;
		resul %= lim;
	}
	return resul;
}

key gerachaves(ll p, ll q){
	key chaves;

	chaves.n = p*q;
	ll phi = (p - 1) * (q - 1);
	euc temp;
	entropy();
	do{
		chaves.e = llrand(phi);
		temp = euclides_ext(chaves.e, phi);
	}while( temp.gcd != 1 );
	chaves.d = temp.inv;
	return chaves;
}

//! Codificacao

vector<ll> stringToBigNum(string str, ll sz){
	vector<ll> vector;
	ll temp = 0;
	for(int i = 0; i < str.length(); i++){
		ll nChar = str[i] - menorChar;
		if(temp * 100 + nChar > powr(10, sz)){
			vector.push_back(temp);
			temp = nChar;
		}else{
			temp = 100 * temp + nChar;
		}
	}
	vector.push_back(temp);
	return vector;
}

string fittosize(ll num, ll size){
	string str = to_string(num);
	while(str.length() < size){
		str = "0" + str;
	}
	return str;
}

string encode(string str, ll n, ll e){
	ll blocksz = (ll) floor(log10(n)) + 1;
	vector<ll> ascii = stringToBigNum(str, blocksz);

	string msgEnc = "";
	for(int i = 0; i < ascii.size(); i++){
		msgEnc += fittosize(modpowr(ascii[i], e, n), blocksz);
	}
	return msgEnc;
}

//! Decodificacao

string blocktostring(vector<ll> vec){
	string resul = "";
	for(int i = 0; i < vec.size(); i++){
		string msg = "";
		while(vec[i] > 0){
			char temp = menorChar + vec[i]%100;
			msg = temp + msg;
			vec[i] = vec[i]/100;
		}
		resul += msg;
	}
	return resul;
}

string decode(string str, ll n, ll d){
	ll blocksz = (ll) floor(log10(n)) + 1;
	vector<ll> blocks;

	for(int i = 0; i < str.length()/blocksz; i++){
		string temp;
		for(int j = 0; j < blocksz; j++){
			temp.push_back(str[i * blocksz + j]);
		}
		blocks.push_back(stoll(temp, nullptr, 10));
	}

	for(int i = 0; i < blocks.size(); i++){
		blocks[i] = modpowr(blocks[i], d, n);
	}

	return blocktostring(blocks);
}

void input(){
	cout<<"Opcoes:\n\t0) Encode\n\t1) Decode"<<endl;
	bool lastIn;
	cin>>lastIn;
	if(lastIn){
		//? Decode
		ll d, n;
		cout<<"N e D?"<<endl;
		cin>>n>>d;
		string msg;
		cout<<"A mensagem criptografada?"<<endl;
		cin>>msg;
		cout<<"A mensagem decriptografada:"<<endl;

		cout<<decode(msg, n, d)<<endl;
	}else{
		//? Encode
		cout<<"Tem chaves?\n0)Nao\n1)Sim"<<endl;
		cin>>lastIn;
		key chaves;
		if(lastIn){
			//? Com chaves
			cout<<"N e E?"<<endl;
			cin>>chaves.n>>chaves.e;
		}else{
			//? Sem chaves
			cout<<"2 primos?"<<endl;
			ll p, q;
			cin>>p>>q;
			chaves = gerachaves(p, q);
			cout<<"Novas chaves:"<<endl;
			cout<<"N = "<<chaves.n<<endl;
			cout<<"E = "<<chaves.e<<endl;
			cout<<"D = "<<chaves.d<<endl;
		}

		string msg;
		cout<<"A mensagem a ser criptografada?"<<endl;
		char clearBuff = getchar();
		while (clearBuff == '\n'){
			clearBuff = getchar();
		}
		ungetc(clearBuff, stdin);
		getline(cin, msg);
		cout<<"A mensagem criptografada:"<<endl;

		cout<<encode(msg, chaves.n, chaves.e)<<endl;
	}
}

int main(){
	input();
	// cout<<modpowr(2, 10, 58921)<<endl;
	// cout<<modpowr(3, 10, 58921)<<endl;
}
